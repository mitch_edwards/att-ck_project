from cal_sdk.cal import CAL, GroundTruth
from flask import Flask, render_template, url_for, request, flash, redirect
from werkzeug.utils import secure_filename
import os, json

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = {'json', 'txt'}

ATTACK_JSON_LOC = './attack_json.json'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


BITE = {}


def create_tagged_json(index=0, selected='', filename=''):
    folder = './tagged/'
    filename_new = filename.replace('.json','')+'_tagged.json'

    f = open(folder+filename_new, 'w+')
    untagged = open('./uploads/'+filename, 'r')
    untagged_json = json.loads(untagged.read())
    try:
        in_question = untagged_json[index]
        print('[-] Successfully loaded json and found the index in question!')
        
        new_json = {}
        new_json['raw_text'] = in_question['content']

        ### TAGGING INFO HERE

        research_metadata = {}
        research_metadata['title'] = in_question['title']
        
        
        print('[-] Loaded JSON!')

    except Exception as e:
        print(f'[x] Exception: {e}\n\tIndex error...\n\tFilename: {filename}\n\tIndex: {index}\n\tSelected: {selected}')
    f.close()
    untagged.close()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def parseFile(filename = ''):
    file_raw = open('./uploads/'+filename, 'r')
    try:
        file_json = json.loads(file_raw.read())
    except:
        return 1, 'File must be .JSON and formatted correctly!'
    #print(f'[-] File_JSON: {file_json}')
    
    return 0, 'File successfully parsed!', file_json
app = Flask(__name__)
@app.route('/test')
def testroute():
    return 'Yah yeet'

"""
TODO: 
- Write a file picker front page
    - Create the filepick.html page
    - Figure out how to send a file via HTML
    - Figure out how to parse a file via Flask
- Write a dashboard that displays all the datas
- Write some JavaScript that queries for data highlighting 
- Write buttons that save the tag->highlighted data pairs
- Write a submit button that saves all the information to Submittable JSON files. 


"""
    
@app.route('/')

def slash():
    return render_template('filepick.html')



@app.route('/documentpost', methods=['POST', 'GET'])
def documentpost():
    buttons = []
    tags = json.loads(open(ATTACK_JSON_LOC, 'r').read())
    for item in tags['tag_objects']:
        #print(item)
        buttons.append(item)
    if request.method == 'POST':
        upload = request.files['file']
        index = 0
        if upload.filename == '':
                flash('No selected file')
                return redirect(request.url)
        if upload and allowed_file(upload.filename):
            filename = secure_filename(upload.filename)
            upload.save(os.path.join(UPLOAD_FOLDER, filename))
        status_code, status, bite = parseFile(filename)
        if status_code == 0:
            return render_template('dash.html', bite = bite, index = 0, filename = upload.filename, buttons = buttons)
    else:
        try:
            selected = request.args['target-text']
            print(f'[-] Selected text: {selected}')
            filename = request.args['filename']
                
            index = int(request.args['index'])
            create_tagged_json(filename=filename, index=index, selected=selected)
        except Exception as e:
            try:
                index = int(request.args['index'])
                filename = request.args['filename']
                f = open(f'./uploads/{filename}', 'r')
                bite = json.loads(f.read())
                f.close()
                if index > len(bite)-1:
                    print('[-] End of bite!')
                    return '<h1> End of bite! </h1>'
                return render_template('dash.html', index=index, bite=bite, filename = filename, buttons = buttons) 


            except Exception as e:
                print(f'[x] Exception: {e}')
            index = int(request.args['index'])+1
            filename = request.args['filename']
            f = open(f'./uploads/{filename}', 'r')
            bite = json.loads(f.read())
            f.close()
            if index > len(bite)-1:
                print('[-] End of bite!')
                return '<h1> End of bite! </h1>'
        
            

            return render_template('dash.html', index=index, bite=bite, filename = filename, buttons = buttons) 


@app.route('/tagged', methods=['POST'])
def tagged():

    tagged_data = json.loads(request.data)
    print(f'Tagged data: {tagged_data}0')

    return '200'