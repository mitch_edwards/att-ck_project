# -*- coding: utf-8 -*-
"""CAL API client including authentication."""
# standard library
import base64
import hashlib
import hmac
import time
from typing import Dict, List

# third-party
from requests import PreparedRequest
from requests.auth import AuthBase
from tcex.sessions import ExternalSession
import traceback

# first-party
from cal_sdk.cal_types import CALDetailsResponse
from cal_sdk.cal_types import CALCountResponse
from cal_sdk.cal_types import CALDataResponse
from cal_sdk.cal_types import CALListResponse


class CALAuth(AuthBase):
    """Token-based auth for CAL."""

    def __init__(self, token: str, timestamp: str):
        """Token authentication for CAL.

        Args:
            token: authorization token from CAL.
            timestamp: token timestamp.
        """
        self.token = token
        self.timestamp = timestamp

    def __call__(self, r: PreparedRequest):
        """Add authorization headers to a CAL request.

        Args:
            r: the request to be sent.

        Returns:
            Request with CAL auth headers added.
        """
        r.headers['Authorization'] = self.token
        r.headers['Timestamp'] = self.timestamp
        return r


def create_cal_token_and_timestamp(license_key: str, instance_id: str) -> CALAuth:
    """Create a CALAuth object from TC license info.

    Both of the arguments can be found in a TC license file.
    Args:
        license_key: License key from license xml file.
        instance_id: Instance ID from license xml file.

    Returns:
        CALAuth object.
    """
    tc_cal_timestamp = str(int(time.time()))

    signature = f'{instance_id}:{tc_cal_timestamp}'
    hmac_signature = hmac.new(
        license_key.encode(), signature.encode(), digestmod=hashlib.sha256
    ).digest()
    tc_cal_token = f'HELIXTOKEN {signature}:{base64.b64encode(hmac_signature).decode()}'

    return CALAuth(tc_cal_token, tc_cal_timestamp)


class CAL:
    """Client for CAL API."""

    def __init__(
        self,
        cal_host: str,
        cal_token: str,
        cal_timestamp: str,
        session: ExternalSession,
        verify_ssl=True,
    ):
        """SDK for the CAL API.

        Args:
            cal_host: CAL instance host.
            cal_token: authorization token from CAL.
            cal_timestamp: token timestamp.
            session: Session to use to communicate with CAL.
            verify_ssl: whether or not to verify CAL's SSL certificate.
        """
        self._session = session
        self._session.base_url = f'https://{cal_host}'
        self._session.verify = verify_ssl
        self._session.auth = CALAuth(cal_token, cal_timestamp)

    def get_list(self) -> CALListResponse:
        response = {
            "pivots": [
                {
                    "label": "Parent Domain",
                    "id": "subdomain_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Subdomains",
                    "id": "subdomain_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Nameservers",
                    "id": "nameserver_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "Nameserver Clients",
                    "id": "nameserver_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "DNS Resolutions",
                    "id": "resolvesto_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "Resolved Domains",
                    "id": "resolvesto_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Address"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "Email Host",
                    "id": "emailhost_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "EmailAddress"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Known Email Addresses",
                    "id": "emailhost_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Base Host",
                    "id": "urlhost_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "URL"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Known URLs",
                    "id": "urlhost_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Base URL",
                    "id": "queryext_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "URL"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Known URL Extensions",
                    "id": "queryext_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "URL"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "CIDR Ranges",
                    "id": "cidrmember_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Address"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "Member IPs",
                    "id": "cidrmember_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "CIDR"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        }
                    ]
                },
                {
                    "label": "CIDR Ranges",
                    "id": "asncidr_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "ASN"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "Known ASNs",
                    "id": "asncidr_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "CIDR"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "WHOIS Registrants",
                    "id": "whoisregistrant_src",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "Host"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                },
                {
                    "label": "Registered Domains",
                    "id": "whoisregistrant_dst",
                    "supportedEntities": [
                        {
                            "type": "Indicator",
                            "subtype": "EmailAddress"
                        }
                    ],
                    "sortingOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score"
                        },
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen"
                        }
                    ],
                    "filterOptions": [
                        {
                            "label": "CAL Score",
                            "id": "score",
                            "datatype": "number",
                            "min": 0,
                            "max": 1000
                        },
                        {
                            "label": "Indicator Status",
                            "id": "indicatorStatus",
                            "datatype": "boolean"
                        },
                        {
                            "label": "First Time Seen",
                            "id": "firstSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Last Time Seen",
                            "id": "lastSeen",
                            "datatype": "timestamp"
                        },
                        {
                            "label": "Lastest Sighting",
                            "id": "latest",
                            "datatype": "boolean"
                        },
                        {
                            "label": "Times Seen",
                            "id": "count",
                            "datatype": "number"
                        }
                    ]
                }
            ],
            "actions": [],
            "entities": [
                {
                    "type": "Indicator",
                    "subtype": "Host",
                    "icon": "path",
                    "reputation": {
                        "min": 0,
                        "max": 1000
                    },
                    "flags": [
                        {
                            "label": "Status",
                            "id": "indicatorStatus"
                        }
                    ],
                    "displaySections": [
                        {
                            "label": "Fields",
                            "id": "fields",
                            "displayType": "keyValues",
                            "values": [
                                {
                                    "label": "Summary",
                                    "id": "summary",
                                    "datatype": "string"
                                },
                                {
                                    "label": "First Seen",
                                    "id": "firstSeen",
                                    "datatype": "datetime"
                                },
                                {
                                    "label": "Last Seen",
                                    "id": "lastSeen",
                                    "datatype": "datetime"
                                }
                            ]
                        }
                    ],
                    "defaultSizes": {}
                },
                {
                    "type": "Indicator",
                    "subtype": "Address",
                    "icon": "path",
                    "displayFields": "TBD"
                },
                {
                    "type": "Indicator",
                    "subtype": "EmailAddress",
                    "icon": "path",
                    "displayFields": "TBD"
                },
                {
                    "type": "Indicator",
                    "subtype": "URL",
                    "icon": "path",
                    "displayFields": "TBD"
                },
                {
                    "type": "Indicator",
                    "subtype": "CIDR",
                    "icon": "path",
                    "displayFields": "TBD"
                },
                {
                    "type": "Indicator",
                    "subtype": "ASN",
                    "icon": "path",
                    "displayFields": "TBD"
                }
            ]
        }
        return CALListResponse(response)

    def get_details(self, indicators: List[Dict[str, str]]) -> List[CALDetailsResponse]:
        """Send indicators to CAL to get details.

        Args:
            indicators: Indicators to send to cal, expected to be in the format:
                {
                    'uniqueId': '127.0.0.1',
                    'indicatorType': 'Address'
                }

        Returns:
            A list of CALDetailResponses.

        Raises:
            HTTPError on a non-successful response from CAL.
        """
        response = self._session.post(
            '/helix/indicators/v4/details?source=playbooks',
            headers={'Content-Type': 'application/json', 'Cache-Control': 'no-cache'},
            json=indicators,
        )
        response.raise_for_status()

        return [CALDetailsResponse(d) for d in response.json()]

    def get_data(self, indicators: List[Dict[str, str]]) -> List[CALDataResponse]:
        """Send indicators to CAL to get details.

        Args:
            indicators: Indicators to send to cal, expected to be in the format:
                {
                    'uniqueId': '127.0.0.1',
                    'indicatorType': 'Address'
                }

        Returns:
            A list of CALCountResponses.

        Raises:
            HTTPError on a non-successful response from CAL.
        """

        try:
            path = '/helix/relationship/v4/data'
            response = self._session.get(
                path,
                headers={'Content-Type': 'application/json', 'Cache-Control': 'no-cache'},
                json=indicators,
            )
            response.raise_for_status()
            response_json = response.json()
        except:
            traceback.print_exc()
            response_json = []

        return [CALDataResponse(d) for d in response_json]

    def get_count(self, indicators: List[Dict[str, str]]) -> List[CALCountResponse]:
        """Send indicators to CAL to get details.

        Args:
            indicators: Indicators to send to cal, expected to be in the format:
                {
                    'uniqueId': '127.0.0.1',
                    'indicatorType': 'Address'
                }

        Returns:
            A list of CALCountResponses.

        Raises:
            HTTPError on a non-successful response from CAL.
        """

        try:
            path = '/helix/relationship/v4/counts'
            response = self._session.get(
                path,
                headers={'Content-Type': 'application/json', 'Cache-Control': 'no-cache'},
                json=indicators,
            )
            print('response: ', response.request.url)
            response.raise_for_status()
            response_json = response.json()
        except:
            traceback.print_exc()
            response_json = []

        return [CALCountResponse(d) for d in response_json]
