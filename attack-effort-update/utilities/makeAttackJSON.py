DELIMITER = ' - '
LIST_FILE_LOC = './attack_object_list.txt'
JSON_FILE_LOC = './attack_json.json'

import json
def main():
    read = open(LIST_FILE_LOC, 'r')
    write = open(JSON_FILE_LOC, 'w')

    root_obj = {}
    root_list = []

    for line in read.readlines():
        if len(line) > 1:
            try:
                split = line.strip().split(DELIMITER)
                line_obj = {}
                line_obj['tag'] = split[0]
                line_obj['description'] = split[1]
                root_list.append(line_obj)
            except:
                print(line)
    root_obj['tag_objects'] = root_list
    write.write(json.dumps(root_obj))

    read.close()
    write.close()

main()
