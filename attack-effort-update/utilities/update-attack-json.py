import argparse
import json
from pyattck import Attck

def upgrade_json(json_file, save):
  attack = Attck()
  attack.update()

  with open(json_file, 'r') as jf:
      data = json.loads(jf.read())

  for idx, entry in enumerate(data['tag_objects']):
      print(f"Updating {entry['tag']}")
      try:
        tech = list(filter(lambda x: x.id == entry['tag'], attack.enterprise.techniques))[0]
        data['tag_objects'][idx]['details'] = tech.description
        if entry['description'] != tech.name:
            print(f"[!]     Names do not match: json file says '{entry['description']}' pyattck says '{tech.name}'")
        print(f"[+]     Updated {tech.id} {tech.name}")
        data['tag_objects'][idx]['link'] = list(filter(lambda x: x['source_name'] == 'mitre-attack', tech.reference))[0]['url']
      except:
        data['tag_objects'][idx]['details'] = ''
        data['tag_objects'][idx]['link'] = f'https://attack.mitre.org/techniques/{entry["tag"]}/'
        print(f"[*]     Error updating, skipping {entry['description']}")
      print('\n')

  if save:
    print(f'Saving updates back to file')
    with open(json_file, 'w') as jf:
      jf.write(json.dumps(data))


if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('file', help="The attack json file to update")
  parser.add_argument('-s', help="Update the json file on disk", action='store_true')

  args = parser.parse_args()

  upgrade_json(args.file, args.s)

