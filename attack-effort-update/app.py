#from cal_sdk.cal import CAL, GroundTruth
from flask import Flask, render_template, url_for, request, flash, redirect
from werkzeug.utils import secure_filename
import os, json
import re

UPLOAD_FOLDER = './uploads'
ALLOWED_EXTENSIONS = {'json', 'txt'}

ATTACK_JSON_LOC = './attack_json.json'

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.secret_key = "asdflkjasfldkj2o3409ouxcvjo*U(OUOIJHNKjnbkjwhberkjnlNkGUR&^$%^#$^%#E%^TYFCHG%^ERTCVJHB"


BITE = {}

def sorted_tags(l):
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)


def create_tagged_json(index=0, selected='', filename=''):
    folder = './tagged/'
    filename_new = filename.replace('.json','')+'_tagged.json'

    f = open(folder+filename_new, 'w+')
    untagged = open('./uploads/'+filename, 'r')
    untagged_json = json.loads(untagged.read())
    try:
        in_question = untagged_json[index]
        print('[-] Successfully loaded json and found the index in question!')

        new_json = {}
        new_json['raw_text'] = in_question['content']

        ### TAGGING INFO HERE

        research_metadata = {}
        research_metadata['title'] = in_question['title']


        print('[-] Loaded JSON!')

    except Exception as e:
        print(f'[x] Exception: {e}\n\tIndex error...\n\tFilename: {filename}\n\tIndex: {index}\n\tSelected: {selected}')
    f.close()
    untagged.close()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def parseFile(filename = ''):
    file_raw = open('./uploads/'+filename, 'r')
    try:
        file_json = json.loads(file_raw.read())
    except:
        return 1, 'File must be .JSON and formatted correctly!'
    #print(f'[-] File_JSON: {file_json}')

    return 0, 'File successfully parsed!', file_json

app = Flask(__name__)
@app.route('/test')
def testroute():
    return 'Yah yeet'

"""
TODO:
- Write a file picker front page
    - Create the filepick.html page
    - Figure out how to send a file via HTML
    - Figure out how to parse a file via Flask
- Write a dashboard that displays all the datas
- Write some JavaScript that queries for data highlighting
- Write buttons that save the tag->highlighted data pairs
- Write a submit button that saves all the information to Submittable JSON files.


"""

@app.route('/')
def slash():
    return render_template('filepick.html')


@app.route('/documentpost', methods=['POST', 'GET'])
def documentpost():

    buttons = []
    ids = []
    idToD = {}
    with open(ATTACK_JSON_LOC, 'r') as attack_json:
      tags = json.loads(attack_json.read())

    for item in tags['tag_objects']:
        ids.append(item['tag'])
        idToD[item['tag']] = item['description']


    ids = sorted_tags(ids)
    for id in ids:
        buttons.append({"tag" : id, "description" : idToD[id], 'details': list(filter(lambda x: x['tag'] == id, tags['tag_objects']))[0]['details'], 'link': list(filter(lambda x: x['tag'] == id, tags['tag_objects']))[0]['link']})
    #print(buttons)

    if request.method == 'POST':
        upload = request.files['file']
        index = 0
        if upload.filename == '':
                flash('No selected file')
                return redirect(request.url)
        if upload and allowed_file(upload.filename):
            filename = secure_filename(upload.filename)
            upload.save(os.path.join(UPLOAD_FOLDER, filename))
        status_code, status, bite = parseFile(filename)
        if status_code == 0:
            return render_template('dash.html', bite = bite, index = 0, filename = upload.filename, buttons = buttons)
    


    return render_template('dash.html', index=index, bite=bite, filename = filename, buttons = buttons)

@app.route('/dash', methods=['POST'])
def dash():
    buttons = []
    ids = []
    idToD = {}

    with open(ATTACK_JSON_LOC, 'r') as attack_json:
      tags = json.loads(attack_json.read())

    for item in tags['tag_objects']:
        ids.append(item['tag'])
        idToD[item['tag']] = item['description']


    ids = sorted_tags(ids)
    for id in ids:
        buttons.append({"tag" : id, "description" : idToD[id], 'details': list(filter(lambda x: x['tag'] == id, tags['tag_objects']))[0]['details'], 'link': list(filter(lambda x: x['tag'] == id, tags['tag_objects']))[0]['link']})
    #print(str(request.form))
    data = request.form
    index = int(data['index'])+1
    
    #print('[-] Bite: '+data['bite'])
    filename = data['filename']
    
    status_code, status, bite = parseFile(filename)
    
    return render_template('dash.html', index=index, bite=bite, filename=filename, buttons=buttons)
@app.route('/tag', methods=['POST'])
def tag():
    data = json.loads(request.data)
    print(str(data))
    for item in data:
        tag = item['tag']
        tagged_text = item['tagged_text']
        filename = item['filename']
        index = item['index']

        obj = {'tag':tag, 'filename':filename, 'tagged_text':tagged_text}
        w = open('./tagged/'+filename+'_'+index+'.json', 'a+')
        json.dump(obj, w)
        w.write(',')
        w.close()
    
    return 'Success'

if __name__ == '__main__':
    app.run(host='localhost')
